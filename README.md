# 1.感谢
感谢小伙伴儿的分享：
● https://gitee.com/buji-qiang
● https://gitee.com/guo-zhongtian
# 2.包含的工具类
● WordToPdfUtil用于将word文档转换为pdf格式的工具类
● ExcelToPdfUtil用于将excel文档转换为pdf格式的工具类
● PdfToImageUtil用于将pdf文档转换为image格式的工具类
# 3.lib文件说明
## 3.1 使用的jar
● aspose-words-15.8.0-jdk16.jar 将word文档转换为pdf需要引入
● aspose-cells-8.5.2.jar 将excel文档转换为pdf需要引入
## 3.2 未使用的jar
● aspose-words-15.12.0-jdk16.jar 未测试
● aspose-pdf-22.4.cracked.jar 将pdf转换为其他格式【破解版效果不佳】
● aspose-pdf-22.4.jar 将pdf转换为其他格式【未破解效果依然不佳】
