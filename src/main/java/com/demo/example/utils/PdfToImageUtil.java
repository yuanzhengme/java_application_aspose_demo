package com.demo.example.utils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * pdf转换为image的工具类
 * <p>
 * 无私的小伙伴儿们
 * https://gitee.com/buji-qiang/pdf-word-image
 * https://gitee.com/guo-zhongtian/convert-excel-to-pdf
 *
 * @author yz
 * @date 2024/01/22
 */
public class PdfToImageUtil {

    /**
     * 图片类型JPG后缀
     */
    private static final String IMAGE_TYPE_JPG = "jpg";


    /**
     * 根据参数将指定页的PDF转换为Image（默认转换第一页）
     *
     * @param pdfFilePath PDF文件路径
     */
    public static void convertFirstPage(String pdfFilePath) {
        convertPageOfIndex(pdfFilePath, null, null, 0, 1, null);
    }


    /**
     * 根据参数将指定页的PDF转换为Image
     *
     * @param pdfFilePath  PDF文件路径
     * @param indexOfStart PDF的开始页（第一页为0）
     * @param indexOfEnd   PDF的结束页
     */
    public static void convertPageOfIndex(String pdfFilePath, int indexOfStart, int indexOfEnd) {
        convertPageOfIndex(pdfFilePath, null, null, indexOfStart, indexOfEnd, null);
    }

    /**
     * 根据参数将指定页的PDF转换为Image
     *
     * @param pdfFilePath   PDF文件路径
     * @param imageFileDir  图片存储目录
     * @param imageFileName 图片存储文件没
     * @param indexOfStart  PDF的开始页（第一页为0）
     * @param indexOfEnd    PDF的结束页
     * @param type          图片类型
     */
    public static void convertPageOfIndex(String pdfFilePath, String imageFileDir, String imageFileName, int indexOfStart, int indexOfEnd, String type) {
        System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
        int pageNum = 1;
        // 图片类型
        if (type == null || "".equals(type)) {
            type = IMAGE_TYPE_JPG;
        }
        // 1.加载PDF文件
        File file = new File(pdfFilePath);
        // 2.生成JPG图片的文件夹
        imageFileDir = imageFileDir == null ? getImageFileDir(pdfFilePath) : imageFileDir;
        imageFileName = imageFileName == null ? getImageFileName(pdfFilePath) : imageFileName;
        try {
            PDDocument pdDocument = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(pdDocument);
            int pageCount = pdDocument.getNumberOfPages();
            if (indexOfEnd > pageCount) {
                indexOfEnd = pageCount;
            }
            if (indexOfStart >= 0 && indexOfEnd >= 0 && indexOfEnd >= indexOfStart) {
                if (indexOfEnd - indexOfStart == pageNum) {
                    BufferedImage image = renderer.renderImageWithDPI(indexOfStart, 144);
                    ImageIO.write(image, type,
                            new File(imageFileDir.concat(File.separator).concat(imageFileName).concat(".").concat(type)));
                } else {
                    for (int i = indexOfStart; i < indexOfEnd; i++) {
                        BufferedImage image = renderer.renderImageWithDPI(i, 144);
                        ImageIO.write(image, type,
                                new File(imageFileDir.concat(File.separator).concat(imageFileName).concat("_")
                                        .concat(String.valueOf(i + 1)).concat(".").concat(type)));
                    }
                }
            } else {
                convertAllPage(pdfFilePath, imageFileDir, imageFileName, type);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据参数将全部的PDF转换为Image
     *
     * @param pdfFilePath PDF文件路径
     */
    public static void convertAllPage(String pdfFilePath) {
        convertAllPage(pdfFilePath, null, null, null);
    }

    /**
     * 根据参数将全部的PDF转换为Image
     *
     * @param pdfFilePath   PDF文件路径
     * @param imageFileDir  图片存储目录
     * @param imageFileName 图片存储文件没
     * @param type          图片类型
     */
    public static void convertAllPage(String pdfFilePath, String imageFileDir, String imageFileName, String type) {
        System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
        // 图片类型
        if (type == null || "".equals(type)) {
            type = IMAGE_TYPE_JPG;
        }
        // 1.加载PDF文件
        File file = new File(pdfFilePath);
        // 2.生成JPG图片的文件夹
        imageFileDir = imageFileDir == null ? getImageFileDir(pdfFilePath) : imageFileDir;
        imageFileName = imageFileName == null ? getImageFileName(pdfFilePath) : imageFileName;
        try {
            PDDocument pdDocument = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(pdDocument);
            int pageCount = pdDocument.getNumberOfPages();

            for (int i = 0; i < pageCount; i++) {
                BufferedImage image = renderer.renderImageWithDPI(i, 144);
                ImageIO.write(image, type,
                        new File(imageFileDir.concat(File.separator).concat(imageFileName).concat("_")
                                .concat(String.valueOf(i + 1)).concat(".").concat(type)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取生成的 image 文件路径，默认与源文件同一目录
     *
     * @param pdfFilePath pdf文件
     * @return 生成的 image 文件路径
     */
    private static String getImageFileDir(String pdfFilePath) {
        int lastIndexOfPoint = pdfFilePath.lastIndexOf("/") == -1 ? pdfFilePath.lastIndexOf("\\") : pdfFilePath.lastIndexOf("/");
        String imageFileDir = "";
        if (lastIndexOfPoint > -1) {
            imageFileDir = pdfFilePath.substring(0, lastIndexOfPoint);
        }
        return imageFileDir;
    }

    /**
     * 获取生成的 image 文件名称，默认与源文件命名一致
     *
     * @param pdfFilePath pdf文件
     * @return 生成的 image 文件
     */
    private static String getImageFileName(String pdfFilePath) {
        int lastIndexOfPath = pdfFilePath.lastIndexOf("/") == -1 ? pdfFilePath.lastIndexOf("\\") : pdfFilePath.lastIndexOf("/");
        int lastIndexOfPoint = pdfFilePath.lastIndexOf(".");
        String imageFileName = "";
        if (lastIndexOfPath > -1 && lastIndexOfPoint > -1) {
            imageFileName = pdfFilePath.substring(lastIndexOfPath + 1, lastIndexOfPoint);
        }
        return imageFileName;
    }

}
