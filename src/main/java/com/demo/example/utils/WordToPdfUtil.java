package com.demo.example.utils;


import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * word转换为pdf的工具类
 * <p>
 * 无私的小伙伴儿们
 * https://gitee.com/buji-qiang/pdf-word-image
 * https://gitee.com/guo-zhongtian/convert-excel-to-pdf
 *
 * @author yz
 * @date 2024/01/22
 */
public class WordToPdfUtil {

    /**
     * 许可证字符串
     */
    private static final String LICENSE_STR = "<License>" +
            "<Data>" +
            "<Products><Product>Aspose.Total for Java</Product><Product>Aspose.Words for Java</Product></Products>" +
            "<EditionType>Enterprise</EditionType>" +
            "<SubscriptionExpiry>20991231</SubscriptionExpiry>" +
            "<LicenseExpiry>20991231</LicenseExpiry>" +
            "<SerialNumber>8bfe198c-7f0c-4ef8-8ff0-acc3237bf0d7</SerialNumber>" +
            "</Data>" +
            "<Signature>sNLLKGMUdF0r8O1kKilWAGdgfs2BvJb/2Xp8p5iuDVfZXmhppo+d0Ran1P9TKdjV4ABwAgKXxJ3jcQTqE/2IRfqwnPf8itN8aFZlV3TJPYeD3yWE7IT55Gz6EijUpC7aKeoohTb4w2fpox58wWoF3SNp6sK6jDfiAUGEHYJ9pjU=</Signature>" +
            "</License>";

    /**
     * word 转 pdf
     *
     * @param wordFilePath word文件路径
     */
    public static void convert(String wordFilePath) {
        convert(wordFilePath, null);
    }


    /**
     * word 转 pdf
     *
     * @param wordFilePath word文件路径
     * @param pdfFilePath  pdf文件路径
     */
    public static void convert(String wordFilePath, String pdfFilePath) {
        FileOutputStream fileOutputStream = null;
        try {
            pdfFilePath = pdfFilePath == null ? getPdfFilePath(wordFilePath) : pdfFilePath;
            setLicense();
            File file = new File(pdfFilePath);
            fileOutputStream = new FileOutputStream(file);
            Document doc = new Document(wordFilePath);
            doc.save(fileOutputStream, SaveFormat.PDF);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert fileOutputStream != null;
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * 获取 生成的 pdf 文件路径，默认与源文件同一目录
     *
     * @param wordFilePath word文件
     * @return 生成的 pdf 文件
     */
    private static String getPdfFilePath(String wordFilePath) {
        int lastIndexOfPoint = wordFilePath.lastIndexOf(".");
        String pdfFilePath = "";
        if (lastIndexOfPoint > -1) {
            pdfFilePath = wordFilePath.substring(0, lastIndexOfPoint);
        }
        return pdfFilePath + ".pdf";
    }

    /**
     * 设置 license 去除水印
     */
    private static void setLicense() {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(LICENSE_STR.getBytes());
        License license = new License();
        try {
            license.setLicense(byteArrayInputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
